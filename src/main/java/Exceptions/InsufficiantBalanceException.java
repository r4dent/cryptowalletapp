package Exceptions;

public class InsufficiantBalanceException extends Exception {
    public InsufficiantBalanceException() {
        super("Insufficient Account Balance");
    }
}
